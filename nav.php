<nav>
    <a class="<?php
                if (PATH_PARTS['filename'] == "index") {
                    print 'activePage';
                }
                ?>" href="index.php">Home</a>

    <a class="<?php
                if (PATH_PARTS['filename'] == "about") {
                    print 'activePage';
                }
                ?>" href="about.php">About</a>

    <a class="<?php
                if (PATH_PARTS['filename'] == "contact") {
                    print 'activePage';
                }
                ?> " href="contact.php">Contact</a>
<!-- https://www.w3schools.com/howto/howto_css_dropdown_navbar.asp -->
    <div class="dropdown">
        <a class="dropbtn <?php
                            if (PATH_PARTS['filename'] == "admin") {
                                print 'activePage';
                            }
                            ?> " href="admin.php">Admin
        </a>
        <div class="dropdown-content">
            <a href="admin/wildlifeForm.php">Insert New Wildlife</a>
            <a href="admin/updateList.php?action=u">Update Wildlife</a>
            <a href="admin/updateList.php?action=d">Delete Wildlife</a>
        </div>
    </div>
</nav>