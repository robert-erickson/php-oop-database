<footer>
    <p>Robert Erickson's  <a href="../index.php">Site map (Main index)</a></p>
    <p><cite>Information and images from <a href="https://vtfishandwildlife.com/learn-more/vermont-critters" target="_blank">VT Fish and Wildlife</a></cite></p>
</footer>
<?php
if(PATH_PARTS['filename'] == 'adoptCritter'){
    print '<script src="javascript/custom.js"></script>';
}
?>
</body>
</html>